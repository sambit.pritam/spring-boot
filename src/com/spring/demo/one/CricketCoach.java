package com.spring.demo.one;

public class CricketCoach implements Coach {

	private FortuneService fortuneService;
	
	// add fields for emailAddress and team
	private String emailAddress;
	private String team;
	
	// no-arg constructor
	public CricketCoach( ) {
		System.out.println("CricketCoach: inside no-arg constructor");
	}

	// our setter method
	/**
	 * @param fortuneService the fortuneService to set
	 */
	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("CricketCoach: inside fortuneService setter method");
		this.fortuneService = fortuneService;
	}

	
	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		System.out.println("CricketCoach: inside emailAddress setter method");
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the team
	 */
	public String getTeam() {
		return team;
	}

	/**
	 * @param team the team to set
	 */
	public void setTeam(String team) {
		System.out.println("CricketCoach: inside team setter method");
		this.team = team;
	}

	@Override
	public String getDailyWorkout() {
		
		String workout = "Spend 30 mins on catching practice!";
		return workout;
	}

	@Override
	public String getDailyFortune() {
		return "CricketCoach says: " + fortuneService.getFortune();
	}

}
