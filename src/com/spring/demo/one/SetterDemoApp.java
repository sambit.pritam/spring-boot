package com.spring.demo.one;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SetterDemoApp {

	public static void main(String[] args) {

		
		// load spring configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// retrieve bean from the container
		CricketCoach cricketCoach = context.getBean("myCricketCoach", CricketCoach.class);
		
		// call the methods in the bean
		System.out.println(cricketCoach.getDailyWorkout());
		System.out.println(cricketCoach.getDailyFortune());
		System.out.println("Cricket Coach email address: " + cricketCoach.getEmailAddress());
		System.out.println("Cricket Coach is of Team: " + cricketCoach.getTeam());
		
		// close the context
		System.out.println("----------------Closing Context------------------");
		context.close();
	}

}
