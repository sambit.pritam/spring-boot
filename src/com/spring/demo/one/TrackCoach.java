package com.spring.demo.one;

public class TrackCoach implements Coach {

	private FortuneService fortuneService;
	
	public TrackCoach() {}
	
	public TrackCoach(FortuneService fortuneService) {
		super();
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		
		return "Run hard 5k!";
	}

	@Override
	public String getDailyFortune() {
		return "TrackCoach says: " + fortuneService.getFortune();
	}

}
